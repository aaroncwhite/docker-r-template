FROM rocker/tidyverse:3.6.1 as base

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    libxml2-dev \
    libgit2-dev \
    libpng-dev \
    libudunits2-dev \
    libgdal-dev

# Because the container is single-use, we can use the global library here
RUN export ADD=shiny && bash /etc/cont-init.d/add && \
    R -e 'install.packages("renv")' && \
    echo "RENV_PATHS_LIBRARY=/usr/local/lib/R/library" >> /usr/local/lib/R/etc/Renviron && \
    echo "RENV_PATHS_CACHE=/usr/local/lib/R/library" >> /usr/local/lib/R/etc/Renviron && \
    chown -R rstudio:rstudio /usr/local/lib/R

WORKDIR /home/rstudio
COPY . .
RUN R -e 'renv::restore()'

EXPOSE 8787