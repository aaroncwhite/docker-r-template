# DockerShinyApp

## Installing
This project is built to use Docker and docker-compose to make development easy across all machines and remove host machine configuration as a potential issue.  Use `docker-compose` to get started quickly.  The project uses a pre-build registry 
image, so there is nothing to install beyond docker and docker-compose. 

* [Docker](https://docs.docker.com/install/)
* [Docker Compose](https://docs.docker.com/compose/install/)

Use docker-compose to build the image:
```
docker-compose build
```

Remember that docker images are immutable once built.  Only changes to files in `/home/rstudio` will persist after restarts. 

## Development
To create an RStudio environment preloaded with all dependencies
1. Create a `.env` file that defines the desired RStudio password, e.g.
```
PASSWORD=mystrongpassword
```
There is an example [sample.env](sample.env) you can also use.  Copy it and rename it to `.env`. 

2. Start the environment
`docker-compose up`

3. Visit `http://localhost:8787` and start hacking.

4. Keep shiny app code in `./app`.  Launch it for development with `shiny::runApp('app')`.

### Dependency management
This project is setup with [Renv](https://rstudio.github.io/renv/articles/renv.html) pre-installed.  It is already setup to 
"activate" when you first log into the Rstudio server interface; however, you need to do one more step for your changes to 
persist across container builds. 

Your typical workflow probably looks like this:
```r
> install.packages('lme4')
> library(lme4)
> glmer(y ~ x + (1 | z), ...)
```

Instead of installing to the global R environment, this will install to the [renv](./renv) folder.  You need to update the 
[renv.lock](renv.lock) file to save the changes to the state of the environment.  

Your new workflow should add one new command to the end of that:
```r
> install.packages('lme4')
> renv::snapshot()
> library(lme4)
> glmer(y ~ x + (1 | z), ...)
```

__But that's hard to remember__, so this project space actually overrides the normal `install.packages` function to do both things in 
one step.  Take a look at the [.Rprofile](.Rprofile) file to see how that works. In addition, we're taking advantage of the 
global container environment for the tidyverse related packages installed in the [rocker/tidyverse](https://hub.docker.com/r/rocker/tidyverse).

### renv.lock
Add it to git.  Commit it often (as often as you add new packages).  Doing so ensures the project is *always* able to recreate the same working 
state, regardless of machine where it runs. 

## Deployment
TBD