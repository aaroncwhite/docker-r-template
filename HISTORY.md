# History

## 0.1.1
-----------------------
* Add docker CI
* Fix renv to use local library

## 0.1.0
-----------------------
* Initial setup of project skeleton
